import numpy as np

IDENTITY = np.array([
    [0, 0, 0],
    [0, 1, 0],
    [0, 0, 0]
    ])

EDGE_DETECTION_1 = np.array([
    [1, 0, -1],
    [0, 0, 0],
    [-1, 0, 1]
    ])

EDGE_DETECTION_2 = np.array([
    [0, 1, 0],
    [1, -4, 1],
    [0, 1, 0]
    ])

EDGE_DETECTION_3 = np.array([
    [-1, -1, -1],
    [-1, 8, -1],
    [-1, -1, -1]
    ])

SHARPEN = np.array([
    [0,-1, 0],
    [-1, 5, -1],
    [0, -1, 0]
    ])

NORMALIZATION = np.array([
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1]
    ]) / float(9)

GAUSSIAN_3 = np.array([
    [1, 2, 1],
    [2, 4, 2],
    [1, 2, 1]
    ]) / float(16)

GAUSSIAN_5 = np.array([
    [1, 4, 6, 4, 1],
    [4, 16, 24, 16, 4],
    [6, 24, 36, 24, 6],
    [4, 16, 24, 16, 4],
    [1, 4, 6, 4, 1]
    ]) / float(256)

SOBEL_X = np.array([
    [1, 0, -1],
    [2, 0, -2],
    [1, 0, -1]
    ])

SOBEL_Y = np.array([
    [1,   2,  1],
    [0,   0,  0],
    [-1, -2, -1]
    ])

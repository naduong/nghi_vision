import cython

# import numpy from python and cython
import numpy as np
cimport numpy as np
from libcpp.vector cimport vector as cpp_vector
from libcpp.memory cimport unique_ptr as cpp_unique_ptr
from cython.operator cimport dereference as deref

# declare interface to the C function
cdef extern from "c_matrix.h" namespace "ADC":
    cdef cppclass c_matrix:

        c_matrix(size_t nrows, size_t ncols, double vals);

        c_matrix(size_t nrows, size_t ncols, double* ndarray);

        void scalar_multiply(double scalar);

        cpp_unique_ptr[cpp_vector[cpp_vector[double]]] getMatrix();

        size_t get_nrows();

        size_t get_ncols();

        void display();

        void y_derivative();

        void x_derivative();

        void convolution(c_matrix& kernel);




cdef class py_matrix:
    cdef c_matrix* cMatrix;


    def convert_to_python_np(self):
        cdef cpp_unique_ptr[cpp_vector[cpp_vector[double]]] matrix_ptr;
        matrix_ptr = self.cMatrix.getMatrix();

        cdef cpp_vector[cpp_vector[double]]* matrix = matrix_ptr.get();


        cdef size_t nrows = self.cMatrix.get_nrows();
        cdef size_t ncols = self.cMatrix.get_ncols();

        matrix_output = []
        for i in range (0, nrows):
            for j in range (0, ncols):
                matrix_output.append(matrix[0][i][j])

        matrix_output = np.reshape(matrix_output, (nrows, ncols))
        return matrix_output


    def __cinit__(self, size_t nrows, size_t ncols, double vals):
        self.cMatrix = new c_matrix(nrows, ncols, vals)


    def __cinit__ (self, np.ndarray[double, ndim = 2, mode = 'c'] input not None):

        cdef size_t nrows, ncols;

        nrows, ncols = input.shape[0], input.shape[1]

        self.cMatrix = new c_matrix(nrows, ncols, &input[0, 0])


    def __dealloc__ (self):
        del self.cMatrix


    @cython.boundscheck(False)
    @cython.wraparound(False)
    def scalarMultiply(self, double val):
        self.cMatrix.scalar_multiply(val)


    def getMatrix(self):
        #return self.cMatrix.getMatrix().get()
        return self.convert_to_python_np()


    def display(self):
        self.cMatrix.display()


    def y_derivative(self):
        self.cMatrix.y_derivative()


    def x_derivative(self):
        self.cMatrix.x_derivative()


    @cython.boundscheck(False)
    def convolution(self, np.ndarray[double, ndim = 2, mode = 'c'] kernel not None):
        cdef size_t k_nrows, k_ncols;

        k_nrows, k_ncols = kernel.shape[0], kernel.shape[1]

        cdef c_matrix* kernel_matrix = new c_matrix(k_nrows, k_ncols, &kernel[0, 0])

        self.cMatrix.convolution(deref(kernel_matrix))

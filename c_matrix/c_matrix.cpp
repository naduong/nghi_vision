#include "c_matrix.h"

namespace ADC
{
        using namespace std;

        c_matrix::~c_matrix()
        {
            
        }

        c_matrix::c_matrix(size_t nrows, size_t ncols, double vals)
        {
            _nrows = nrows;
            _ncols = ncols;

            _matrix = std::make_unique<Vec2D<double>>(create2DVector(nrows, ncols, vals));

        }

        c_matrix::c_matrix(size_t nrows, size_t ncols, double* ndarray)
        {
            _nrows = nrows;
            _ncols = ncols;

            _matrix = std::make_unique<Vec2D<double>>(create2DVectorFromNDArray(ndarray, nrows, ncols));
        }

        c_matrix::c_matrix(c_matrix& other){}

        void c_matrix::scalar_multiply(double scalar) {}

        int c_matrix::get_nrows() 
        {
            return _nrows;
        }

        int c_matrix::get_ncols() 
        {
            return _ncols;
        }

         std::unique_ptr<Vec2D<double>> c_matrix::getMatrix() {
            return move(_matrix);
        }

        void c_matrix::display() {
            cout << "\n\n";
            for (size_t i = 0; i < _nrows; ++i)
            {
                for (size_t j = 0; j < _ncols; ++j)
                {
                    cout <<std::to_string((*_matrix)[i][j]) + "  ";
                }
                cout << endl;
            }
            cout << "\n\n";
        }

        void c_matrix::x_derivative() {
            Vec2D<double> derived_matrix = x_derive(*_matrix);
            _matrix = std::make_unique<Vec2D<double>>(derived_matrix);
            _nrows = derived_matrix.size();
            _ncols = derived_matrix[0].size();
        }

        void c_matrix::y_derivative() {
            Vec2D<double> derived_matrix = y_derive(*_matrix);
            _matrix = std::make_unique<Vec2D<double>>(derived_matrix);
            _nrows = derived_matrix.size();
            _ncols = derived_matrix[0].size();
        }

        void c_matrix::convolution(c_matrix& kernel) {
            Vec2D<double> convolved_matrix = convolve(*_matrix, *(kernel.getMatrix()));
            _matrix.release();
            _matrix = std::make_unique<Vec2D<double>>(convolved_matrix);
        }

}
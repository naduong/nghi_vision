#pragma once
#include <memory>
#include <algorithm>
#include <memory>
#include <cmath>
#include <iostream>

namespace ADC
{
    template <class T>
    using Vec2D = std::vector<std::vector<T>>;

    template <class T>
    using Vec2DUniquePtr = std::unique_ptr<Vec2D<T>>;

    template <class T>
    inline void resize_2D_vector(T& vector, size_t nrows, size_t ncols)
    {
        vector.resize(nrows);
        for (size_t i = 0; i < nrows; ++i)
        {
            vector[i].resize(ncols);
        }
    }

    inline Vec2D<double> create2DVectorFromNDArray(double* ndarrayPtr, size_t nrows, size_t ncols)
    {
        Vec2D<double> matrix;
        matrix.resize(nrows);

        for (size_t i = 0; i < nrows; ++i)
        {
            matrix[i].resize(ncols);

            for (size_t j = 0; j < ncols; ++j)
            {
                matrix[i][j] = *(ndarrayPtr++);
            }
        }

        return matrix;
    }

    inline Vec2D<double> create2DVector(size_t nrows, size_t ncols, double val)
    {
        Vec2D<double> matrix;
        matrix.resize(nrows);

        for (size_t i = 0; i < nrows; ++i)
        {
            matrix[i].resize(ncols);

            for (size_t j = 0; j < ncols; ++j)
            {
                matrix[i][j] = val;
            }
        }

        return matrix;
    }

    /*
     * y derivative of matrix -- horizontal derivation
     */
    inline Vec2D<double> x_derive(Vec2D<double> matrix)
    {
        if (&matrix == NULL) 
        {
            std::cout << "Input matrix is NULL.  \n\n";
            return create2DVector(0, 0, 0);
        }

        size_t matrix_nrows = matrix.size();
        size_t matrix_ncols = matrix[0].size();

        //std::cout <<"\t nrows = " << matrix_nrows <<", ncols = " << matrix_ncols << "\n\n";

        Vec2D<double> derived_matrix = create2DVector(matrix_nrows, matrix_ncols - 1, 0);

        for (size_t i = 0; i < matrix_nrows; i++)
        {
            for(size_t j = 0; j < matrix_ncols - 1; j++)
            {
                //std::cout <<"\t\t i = " << i <<", j = " << j << "\n";
                derived_matrix[i][j] = matrix[i][j + 1] - matrix[i][j];
            }
        }

        return derived_matrix;
    }

    /**
     * x derivative of a matrix -- vertical derivation
     */
    inline Vec2D<double> y_derive(Vec2D<double> matrix)
    {
        if (&matrix == NULL) 
        {
            std::cout << "Input matrix is NULL.  \n\n";
            return create2DVector(0, 0, 0);
        }

        size_t matrix_nrows = matrix.size();
        size_t matrix_ncols = matrix[0].size();

        Vec2D<double> derived_matrix = create2DVector(matrix_nrows - 1, matrix_ncols, 0);

        for (size_t i = 0; i < matrix_nrows - 1; i++)
        {
            for(size_t j = 0; j < matrix_ncols; j++)
            {
                derived_matrix[i][j] = matrix[i + 1][j] - matrix[i][j];
            }
        }

        return derived_matrix;
    }

    /**
     * convolution operation
     * Odd size kernel version -- no edge cut off
     */
    inline Vec2D<double> convolve(Vec2D<double> matrix, Vec2D<double> kernel)
    {
        if (&matrix == NULL) 
        {
            std::cout << "Input matrix is NULL.  \n\n";
            return create2DVector(0, 0, 0);
        }

        if (&kernel == NULL) 
        {
            std::cout << "Input kernel is NULL.  \n\n";
            return create2DVector(0, 0, 0);
        }

        size_t matrix_nrows = matrix.size();
        size_t matrix_ncols = matrix[0].size();

        size_t kernel_size = (int)(kernel.size() / 2);

        if (matrix_nrows <= kernel_size || matrix_ncols <= kernel_size)
        {
            std::cout << "kernel is larger than matrix. \n\n";
            return create2DVector(0, 0, 0);
        }

        if (kernel_size == 0 || kernel_size != (int)(kernel[0].size() / 2))
        {
            std::cout << "Kernel needs to be a square matrix. \n\n";
            create2DVector(0, 0, 0);
        }
        int k_i = 0;
        int k_j = 0;
        Vec2D<double> convolved_matrix = create2DVector(matrix_nrows, matrix_ncols, 0);

        for (size_t i = 0; i < matrix_nrows; ++i)
        {
            for (size_t j = 0; j < matrix_ncols; ++j)
            {               
                for (k_i = -kernel_size; k_i <= (int)kernel_size; k_i++)
                {
                    for (k_j = -kernel_size; k_j <= (int)kernel_size; k_j++)
                    {
                        int row_i = i + k_i;
                        int col_j = j + k_j;
                        int element = 0;

                        if (row_i  >= 0 && col_j >= 0 && row_i < matrix_nrows && col_j < matrix_ncols)
                        {
                            element = matrix[row_i][col_j];
                        }
                        convolved_matrix[i][j] += kernel[k_i + kernel_size][k_j + kernel_size] * element;
                    }
                }
            }
        }

        return convolved_matrix;
    }
}

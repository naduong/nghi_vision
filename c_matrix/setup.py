import Cython

from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

import numpy

setup(
  name = 'Demos',
  ext_modules=[
    Extension("matrix",
              sources=["matrix.pyx", "c_matrix.cpp"], # Note, you can link against a c++ library instead of including the source
              include_dirs=[numpy.get_include()],
              language="c++",
              extra_compile_args=["-std=c++14"],
              extra_link_args=["-g"],
              gdb_debug=True),
    ],
  cmdclass = {'build_ext': build_ext},

)

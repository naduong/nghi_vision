#include <iostream>
#include "c_matrix.h"
#include <vector>
#include <memory>

using namespace std;
using namespace ADC;

void test_y_derivative()
{
    //////////////////////////////// 
    double * test1;
    int nrows = 5;
    int ncols = 5;
    test1 = new double[nrows * ncols];

    for (int i = 0; i < nrows; i++)
    {
        for (int j = 0; j < ncols; ++j)
        {
            test1[i * ncols + j] = (i * ncols + j) * i;
        }
    }

    cout << "\n\nprint test1: \n";
    for (int index = 0; index < nrows * ncols; index++)
    {
        cout << test1[index] << "  ";
    }
    cout <<endl;

    c_matrix* test11 = new c_matrix(nrows, ncols, test1);
    test11->display();

    test11->y_derivative();
    test11->display();

    delete[] test1;

    delete test11;
}

void test_convolution_1()
{
    int nrows = 151;
    int ncols = 175;
    double* ndarray;
    ndarray = new double[nrows * ncols];
    for (int i = 0; i < nrows * ncols; i++)
    {
        ndarray[i] = i;
    }

    c_matrix* b = new c_matrix(nrows, ncols, ndarray);
    b->display();

    double* ndarray2;
    ndarray2 = new double[3 * 3];
    for (int i = 0; i < 9; i++)
    {
        ndarray2[i] = i;
    }

    c_matrix* k = new c_matrix(3, 3, ndarray2);
    k->display();

    b->convolution(*k);
    b->display();

    delete[] ndarray2;
    delete[] ndarray;
    delete k, b;

}

int main()
{
    test_y_derivative();
    test_convolution_1();
    return 0;
}

#pragma once
#include "helper.h"
#include <iostream>
#include <string>

namespace ADC
{
    class c_matrix
    {
        public:
            c_matrix();

            ~c_matrix();

            c_matrix(size_t nrows, size_t ncols, double vals);

            c_matrix(size_t nrows, size_t ncols, double* ndarray);

            c_matrix(c_matrix& other);

            void scalar_multiply(double scalar);

            int get_nrows();

            int get_ncols();

            std::unique_ptr<Vec2D<double>> getMatrix();

            void display();

            void y_derivative();

            void x_derivative();

            void convolution(c_matrix& kernel);

        private:
            size_t _nrows;
            size_t _ncols;
            std::unique_ptr<Vec2D<double>> _matrix;
    };
}

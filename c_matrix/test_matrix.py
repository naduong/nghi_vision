import numpy as np
from matrix import py_matrix
import cv2
import convolution_kernel as ck
import math

def normalize_matrix(array):
    maxVal = np.amax(array)
    minVal = np.amin(array)

    for r_i in range(0, len(array)):
        for c_j in range (0, len(array[0])):
            array[r_i, c_j] = ((array[r_i, c_j] - minVal) * (255) / (maxVal - minVal))
    return np.uint8(array)


def get_horizontal_derived_image(image):
    py_mat = py_matrix(image.astype(float))
    py_mat.x_derivative()

    return normalize_matrix(py_mat.convert_to_python_np().astype(int))


def get_vertical_derived_image(image):
    py_mat = py_matrix(image.astype(float))
    py_mat.y_derivative()

    return normalize_matrix(py_mat.convert_to_python_np().astype(int))


def get_convolved_image(image, kernel):
    py_mat = py_matrix(image.astype(float))
    py_mat.convolution(kernel.astype(float))

    return normalize_matrix(py_mat.convert_to_python_np().astype(int))


## NOTE: Need to revisit sobel_mag because it outputs some SH!T, not image
def get_sobel_image(image):
    sobel_x = get_convolved_image(image, ck.SOBEL_X)
    sobel_y = get_convolved_image(image, ck.SOBEL_Y)

    sobel_mag = np.sqrt(sobel_x * sobel_x + sobel_y * sobel_y)
    sobel_grad = np.arctan2(sobel_y, sobel_x)

    return normalize_matrix(sobel_mag), normalize_matrix(sobel_grad)


image = cv2.imread("test1.jpg", cv2.IMREAD_GRAYSCALE)

imMag, imGrad = get_sobel_image(image)

cv2.imwrite("test1_sobelMag.jpg", imMag)
cv2.imwrite("test1_sobelGrad.jpg", imGrad)

